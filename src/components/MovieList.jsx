import MovieItem from "./MovieItem";

function MovieList() {
  const movieList = [
    {
      id: 1,
      title: "Alien",
      description: "ça fait peur",
      poster: "https://m.media-amazon.com/images/M/MV5BOGQzZTBjMjQtOTVmMS00NGE5LWEyYmMtOGQ1ZGZjNmRkYjFhXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_FMjpg_UX1000_.jpg"
    },
    {
      id: 2,
      title: "Back to the Future",
      description: "c'est bien",
      poster: "https://m.media-amazon.com/images/M/MV5BOGQzZTBjMjQtOTVmMS00NGE5LWEyYmMtOGQ1ZGZjNmRkYjFhXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_FMjpg_UX1000_.jpg"
    },
    {
      id: 3,
      title: "The Thing",
      description: "ça fait peur aussi",
      poster: "https://m.media-amazon.com/images/M/MV5BOGQzZTBjMjQtOTVmMS00NGE5LWEyYmMtOGQ1ZGZjNmRkYjFhXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_FMjpg_UX1000_.jpg"
    },
  ];

  return (
    <div>
      {movieList.map((eachMovie) => (
        <MovieItem key={eachMovie.id} {...eachMovie} />
      ))}
    </div>
  );
}

export default MovieList;
