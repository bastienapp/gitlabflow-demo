function MovieItem(props) {
  const { title, description, poster } = props;
  return (
    <div>
      <h3>{title}</h3>
      <p>{description}</p>
      <img src={poster} alt={title} />
    </div>
  );
}

export default MovieItem;
